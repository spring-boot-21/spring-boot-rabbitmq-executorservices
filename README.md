## Pre-requisitos
- Spring Boot 2.6.3
- Spring 5.3.15
- Tomcat embed 9.0.56
- Maven 3.8.4
- Java 11

## Construir proyecto
```
mvn clean install
```

