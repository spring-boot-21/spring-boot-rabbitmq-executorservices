/**
 * 
 */
package com.zademy.rabbit.app.exposicion.configuraciones;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.support.RetryTemplate;

import com.zademy.rabbit.app.persistencia.constantes.RabbitMqConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class RabbitMqConfig.
 */
@Configuration
public class RabbitMqConfig {

	/** The Constant HOST. */
	private static final String HOST = "localhost";

	/** The Constant USER. */
	private static final String USER = "guest";

	/** The Constant PASSWORD. */
	private static final String PASSWORD = "guest";

	/**
	 * Binding.
	 *
	 * @return the binding
	 */
	@Bean
	public Binding binding() {

		return BindingBuilder.bind(queue()).to(exchange()).with(RabbitMqConstants.ROUTING_KEY);

	}

	/**
	 * Connection factory.
	 *
	 * @return the connection factory
	 */
	@Bean
	public ConnectionFactory connectionFactory() {

		CachingConnectionFactory connectionFactory = new CachingConnectionFactory();

		connectionFactory.setHost(HOST);
		connectionFactory.setUsername(USER);
		connectionFactory.setPassword(PASSWORD);

		return connectionFactory;

	}

	/**
	 * Exchange.
	 *
	 * @return the direct exchange
	 */
	@Bean
	public DirectExchange exchange() {

		return new DirectExchange(RabbitMqConstants.EXCHANGE);

	}

	/**
	 * Queue.
	 *
	 * @return the queue
	 */
	@Bean
	public Queue queue() {

		return new Queue(RabbitMqConstants.QUEUE, true);

	}

	/**
	 * Rabbit template.
	 *
	 * @param connectionFactory the connection factory
	 * @return the rabbit template
	 */
	@Bean
	public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {

		ExponentialBackOffPolicy policy = new ExponentialBackOffPolicy();

		policy.setInitialInterval(RabbitMqConstants.INITIAL_INTERVAL);
		policy.setMaxInterval(RabbitMqConstants.MAX_INTERVAL);
		policy.setMultiplier(RabbitMqConstants.MULTIPLIER);

		RetryTemplate retryTemplate = new RetryTemplate();
		retryTemplate.setBackOffPolicy(policy);

		RabbitTemplate template = new RabbitTemplate(connectionFactory);
		template.setRetryTemplate(retryTemplate);

		return template;

	}

}
