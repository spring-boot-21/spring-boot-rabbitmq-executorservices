package com.zademy.rabbit.app;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.zademy.rabbit.app.persistencia.constantes.RabbitMqConstants;
import com.zademy.rabbit.app.servicios.RabbitMqProducerService;

/**
 * The Class RabbitAppApplication.
 */
@EnableRabbit
@SpringBootApplication
public class RabbitAppApplication implements CommandLineRunner {

	/** The rabbit mq producer service. */
	@Autowired
	private RabbitMqProducerService rabbitMqProducerService;

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(RabbitAppApplication.class, args);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.boot.CommandLineRunner#run(java.lang.String[])
	 */
	@Override
	public void run(String... args) throws Exception {

		for (int a = 0; a < 15; a++) {

			rabbitMqProducerService.sendMessage(RabbitMqConstants.QUEUE, "Prueba RabbitMQ ->" + a);

		}

	}

}
