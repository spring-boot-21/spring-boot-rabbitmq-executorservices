/**
 * 
 */
package com.zademy.rabbit.app.persistencia.modelo;

import java.io.Serializable;

/**
 * The Class MessageRequest.
 * 
 * @author Sadot Hernández
 */
public class MessageRequest implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7997131870585713287L;

	/** The message. */
	private String message;

	/**
	 * Instantiates a new message request.
	 */
	public MessageRequest() {
		super();
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MessageRequest [message=");
		builder.append(message);
		builder.append("]");
		return builder.toString();
	}

}
