/**
 * 
 */
package com.zademy.rabbit.app.servicios;

/**
 * The Interface RabbitMqProducerService.
 * 
 * @author Sadot Hernández
 */
public interface RabbitMqProducerService {

	/**
	 * Send message.
	 *
	 * @param message the message
	 */
	public void sendMessage(String queue, String message);

}
