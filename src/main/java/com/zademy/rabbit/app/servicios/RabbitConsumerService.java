/**
 * 
 */
package com.zademy.rabbit.app.servicios;

/**
 * The Interface RabbitConsumerService.
 * 
 * @author Sadot Hernández
 */
public interface RabbitConsumerService {

	/**
	 * Consumer.
	 *
	 * @param message the message
	 */
	void consumer(String message);

}
