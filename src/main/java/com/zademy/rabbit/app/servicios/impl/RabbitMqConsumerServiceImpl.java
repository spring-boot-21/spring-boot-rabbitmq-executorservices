/**
 * 
 */
package com.zademy.rabbit.app.servicios.impl;

import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.zademy.rabbit.app.persistencia.constantes.RabbitMqConstants;
import com.zademy.rabbit.app.servicios.RabbitConsumerService;
import com.zademy.rabbit.app.servicios.utils.Utils;

/**
 * The Class RabbitMqConsumerServiceImpl.
 * 
 * @author Sadot Hernández
 */
@Service
public class RabbitMqConsumerServiceImpl implements RabbitConsumerService {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMqConsumerServiceImpl.class);

	/** The executor service. */
	@Autowired
	@Qualifier("fixedThreadPoolConsumer")
	private ExecutorService executorService;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.rabbit.app.servicios.RabbitConsumerService#listener(java.lang.String)
	 */
	@Override
	@RabbitListener(queues = RabbitMqConstants.QUEUE)
	public void consumer(String message) {
		if (Utils.isNullOrEmpty(message)) {

			LOGGER.info("Message is Null Or Empty: {}", message);

		} else {
			executorService.execute(() -> {

				try {
					LOGGER.info("Consumer received: {}", message);
				} catch (Exception e) {
					LOGGER.error("Error:", e);
				}

			});
		}

	}

}
